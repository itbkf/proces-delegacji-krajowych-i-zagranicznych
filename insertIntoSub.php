<?php
/**
 * Created by PhpStorm.
 * User: Ł.Filipiak
 * Date: 14.12.2017
 * Time: 16:59
 */

class className extends JobRouter\Engine\Runtime\PhpFunction\StepInitializationFunction
{
    const SUBTABLE = 'ST_FOREGIN_PRICE';

    public function execute($rowId = null)
    {


//    //Dla celów testowych.
//
//    $lastDelegationNo = '2016/11/3/LF';
//    $lastDelegationNo = '';

        /** @var START_NUMERACJI $firstNumber */
        $firstNumber = 1;
        // $username = $this->getDialogValue($supervisor);
        $username = 'l.filipiak';
        $year = date('Y');
        $month = date('m');



//    TODO Select aby pobrać z bazy ostatni numer delegacji jeżeli brak wykonuj pętle


        $jobDB = $this->getJobDB();
        $sql = 'SELECT distinct delgation_no FROM BKF_JRDELEGATIONS order by processid desc;';
        $result = $jobDB->query($sql);
        if ($result === false) {
            throw new JobRouterException($jobDB->getErrorMessage());
        }

        $row = $jobDB->fetchRow($result);
        $lastDelegationNo = $row['delgation_no'];




//    Dodaje pierwszy numer jeżeli brak rekordów w bazie

        if ($lastDelegationNo == '') {

            $delegationNo = $year . '/' . $month . '/' . $firstNumber . '/' . strtoupper($username[0]) . strtoupper($username[2]);

            $this->setTableValue('delgation_no', $delegationNo);


//     TODO   Tutaj insert do bazy danych JR

        }

//    Jeżeli numer delegacji nie jest pusty to dokonaj inkrementacji oraz sprawdź czy to nie jest
//      nowy miesiąc

        else
            if ($lastDelegationNo != '') {

                /** @var ARRAY_OF_DELEGATION_NO $splitNo */

                $splitNo = explode('/', $lastDelegationNo);

//    Sprawdza czy nie zmienił się miesiąc

                if ($splitNo[1] != $month) {

                    $newMonth = date('m');
                    $delegationNo = $splitNo[0] . '/' . $newMonth . '/' . $firstNumber . '/' . strtoupper($username[0]) . strtoupper($username[2]);
                    $this->setTableValue('delgation_no', $delegationNo);


                }
//      Sprawdza czt nie zmienił się rok.

                if ($splitNo[0] < $year) {

                    $newYear = date('Y');
                    $delegationNo = $newYear . '/' . $splitNo[1] . '/' . $firstNumber . '/' . strtoupper($username[0]) . strtoupper($username[2]);
                    $this->setTableValue('delgation_no', $delegationNo);

                }
//      Sprawdza czy nie zmienił się i miesiąc.

                if (($splitNo[0] < $year) && $splitNo[1] != $month) {

                    $newYear = date('Y');
                    $newMonth = date('m');

                    $delegationNo = $newYear . '/' . $newMonth . '/' . $firstNumber . '/' . strtoupper($username[0]) . strtoupper($username[2]);
                    $this->setTableValue('delgation_no', $delegationNo);


                }


//      Jeżeli rok i miesiąc są równe bierzącemu nadaj kolejny numer.

                if (($splitNo[0] == $year) && $splitNo[1] == $month) {

                    $newNumber = (int)$splitNo[2] + 1;

                    $delegationNo = $splitNo[0] . '/' . $splitNo[1] . '/' . $newNumber . '/' . strtoupper($username[0]) . strtoupper($username[2]);

                    $this->setTableValue('delgation_no', $delegationNo);


                }

//      TODO zapis nowego numeru do DB.

            }



        $cnt = 0;

        $sql = 'select * from bkf_jrdelegations;';


        $result = $jobDB->query($sql);

        if ($result === false) {
            throw new JobRouterException($jobDB->getErrorMessage());
        }

        while ($row = $jobDB->fetchRow($result)) {

            $this->insertSubtableRow(self::SUBTABLE, ++$cnt, $row);



        }



    }
}
?>