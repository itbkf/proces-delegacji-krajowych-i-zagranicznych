    <?php
    class className extends JobRouter\Engine\Runtime\PhpFunction\RuleExecutionFunction
    {
        public function execute($rowId = null)
        {
            //	Łączę się z bazami danych.
            $jobDB= $this->getJobDB();
            $externalDB = $this->getDBConnection('bkf');

            // 	ProcessID potrzebny do zapytania.
            $processID = $this->getProcessId();
            $stepID = $this->getStepId();
            $username = $this->getUsername();
            $message = $this->getDialogValue('controlling_comment');
            $decission = $this->getDialogValue('controlling_decision');
            $step = $this->getStepLabel();
            $a='';


            foreach ($decission as $key=>$value){

               $a.=$key;
            }

            $sqlInsertBKFTable =
                "INSERT INTO
                    bkf_conversation (conversation_processname, conversation_processid, conversation_stepid,conversation_section,
                        conversation_action,
                        conversation_comment, conversation_adduser, conversation_adddate)
                VALUES
                    ('Delegacje', '".$processID."', '".$stepID."', '".$step."', '".$a."',
                        '".$message."', '".$username."', NOW())";

            //	Wrzucenie wiadomości.
            $result = $externalDB->exec($sqlInsertBKFTable);
        }
    }
    ?>