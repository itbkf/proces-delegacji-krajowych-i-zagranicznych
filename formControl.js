function displaySection() {

    jr_set_disabled('delegation_type', true);
    jr_set_disabled('delegation_option', true);

    let delegation_type =  jr_get_value('delegation_type');
    // alert('W funkcji' + jr_get_value('delegation_option'));

    if(delegation_type == 'Krajowa'){


        jr_show('delegationType');
        jr_show('delegationDetails');
        jr_show('hotel');
        jr_show('transport');
        jr_show('other_costs');
        jr_hide_subtable_column('STV_DELEGATION_DETAIL', 'delegation_country');
        jr_hide_subtable_column('STV_DELEGATION_DETAIL', 'delegation_country2');

    }
    else if(delegation_type == 'Zagraniczna'){


        jr_show('delegationType');
        jr_show('delegationDetails');
        jr_show('hotel');
        jr_show('transport');
        jr_show('other_costs');
        jr_show_subtable_column('STV_DELEGATION_DETAIL', 'delegation_country');
        jr_show_subtable_column('STV_DELEGATION_DETAIL', 'delegation_country2');

    }



}

function block_form(){

    jr_set_readonly('STV_DELEGATION_DETAIL');
    jr_set_readonly('STV_BOOK_FLY');
    jr_set_readonly('STV_CAR_RENT');
    jr_set_readonly('STV_HOTEL_BOOKING');
    jr_set_readonly('STV_OTHER_COST');



}

function add_form(){
    alert('suma');

    let sum =jr_get_value('sum');
    let sum2 =jr_get_value('sum2');
    let sum3 =jr_get_value('sum3');
    let sum4 =jr_get_value('sum4');
    let sum5 =jr_get_value('sum5');
    let sum6 =jr_get_value('instalment_sum');

    let suma = sum + sum2 + sum3 + sum4 + sum5 + sum6;

    jr_set_value('all_sum', suma);


}

function disabled_req() {

    let option = jr_get_value('controlling_decision');

    if(option == 'Tak'){
        jr_set_required('controlling_comment', true);
    }
    else if(option == 'Nie') {
        jr_set_required('controlling_comment', false);
    }
}

function hideInfo(){

    jr_hide('info1');
    jr_hide('info2');
    jr_hide('info3');
    jr_hide('info4');
    jr_hide('info5');
}

function clearField() {


    jr_set_value('controlling_decision','');
    jr_set_value('controlling_comment','');

}

function formControl() {


    var mod=document.getElementById("sum");
    mod.style.border = "#006600 solid 1px";
    mod.style.font="bold 14px arial,serif";

    var mod=document.getElementById("sum2");
    mod.style.border = "#006600 solid 1px";
    mod.style.font="bold 14px arial,serif";

    var mod=document.getElementById("sum3");
    mod.style.border = "#006600 solid 1px";
    mod.style.font="bold 14px arial,serif";

    var mod=document.getElementById("sum4");
    mod.style.border = "#006600 solid 1px";
    mod.style.font="bold 14px arial,serif";

    var mod=document.getElementById("sum5");
    mod.style.border = "#006600 solid 1px";
    mod.style.font="bold 14px arial,serif";

    var mod=document.getElementById("instalment_sum");
    mod.style.border = "#006600 solid 1px";
    mod.style.font="bold 14px arial,serif";

    var mod=document.getElementById("all_sum");
    mod.style.border = "#006600 solid 1px";
    mod.style.font="bold 20px arial,serif";



    let jr_step = jr_get_value('bkf_step');

    if (jr_step == 1) {

        jr_show('info2');
    }

    if (jr_step == 11) {

        jr_show('section10');
        jr_hide('controlling_decision');
        hideInfo();
        jr_hide('info2');
        jr_show('info6');
        jr_select_page('delegationHistory');
        jr_set_label('section10', 'Sekcja weryfikacji dokumentów przez Koordyatora Serwisu');
        clearField();
        displaySection();
        block_form();
    }

    if (jr_step == 4) {
        clearField()
        jr_show('section10');
        hideInfo();
        displaySection();
        jr_select_page('delegationHistory');
        jr_set_label('section10', 'Sekcja decyzji Działu Administracji');

    }

    if (jr_step == 5) {

        clearField()
        jr_show('section10');
        jr_hide('controlling_decision');
        hideInfo();
        displaySection();
        jr_select_page('delegationHistory');
        jr_show('info8');
        jr_set_label('section10', 'Wiadomość dla Działu Administracji');


    }

    if (jr_step == 8) {

        clearField()
        jr_show('section10');
        hideInfo();
        displaySection();
        jr_select_page('delegationHistory');
        jr_set_label('section10', 'Sekcja decyzji Zarządu BKF');
        block_form();


        // jr_show('info8');

    }

    if (jr_step == 6) {

        clearField()
        jr_show('section10');
        hideInfo();
        displaySection();
        jr_select_page('delegationHistory');
        jr_set_label('section10', 'Sekcja decyzji Działu Księgowości');

        // jr_show('info8');

    }

    if (jr_step == 2) {

        clearField()
        jr_show('section10');
        hideInfo();
        displaySection();
        jr_select_page('delegationHistory');
        jr_set_label('section10', 'Sekcja decyzji Kierownika Działu');
        block_form();

    }
    if (jr_step == 3) {

        clearField()
        jr_show('section10');
        hideInfo();
        jr_hide('controlling_decision');

        displaySection();
        jr_select_page('delegationHistory');
        jr_set_label('section10', 'Wiadomość dla Administracji');

        // jr_show('info8');

    }
}




// Funkcja dodająca komunikat
function showInfo(elementId, colorId, message, id, id2) {

    switch(colorId) {

        case 0 :
            color = '#cc0000';
            break;

        case 1 :
            color = '#33cc33';
            break;

        case 2 :
            color = '#ffcc00';
            break;

        case 3 :
            color = '#3399ff';
            break;
    }


    elementId = '#' + elementId;



    message = '<br><p id = "'+ id +'" style = "display: inline; font-weight: bold; color: '
        + color +'">Komunikat: </p><p id = "' + id2 + '" style = "font-weight: bold; display: inline;">' + message + '</p>';


    jQuery(elementId).append(message);
}


// Funkcja ukrywająca komunikat
function hideInfo(id, id2) {

    id = '#' + id;
    id2 = '#' + id2;

    jQuery(id).hide("slow");
    jQuery(id2).hide("slow");


}

function timeCheck() {

    var firstDate = jr_get_subtable_value('STV_DELEGATION_DETAIL', 0, 'time_start');
    var secondDate = jr_get_subtable_value('STV_DELEGATION_DETAIL', 0, 'time_stop');
    var calc = jr_date_diff(secondDate, firstDate, 'h');

}

function TableLoop()
{
    jr_loop_table('STV_DELEGATION_DETAIL', checkVar);
}

function HotelLoop()
{
    jr_loop_table('STV_HOTEL_BOOKING', checkHotel);
}

// Obsługa listy Rodzaj delegacji

// function checkDelegationType() {
//
//     var delegationType = jr_get_value('delegation_type');
//
//
//     if(delegationType == 0){
//
//
//
//     }
// }

// Funkcja obsługująca tabele Szczegóły delegacji STV_DELEGATION_DETAIL



function checkVar(table, rowId) {

    let max_deleagation = jr_get_subtable_max_id(table);
    // alert(max_deleagation);

    var firstDate = jr_get_subtable_value(table, 0, 'time_start');
    var secondDate = jr_get_subtable_value(table, rowId, 'time_stop');


    // let max_id = jr_get_subtable_max_id(table,'time_stop');
    //
    //
    //     var firstDate = jr_get_subtable_value(table, rowId, 'time_start');
    //     var secondDate = jr_get_subtable_value(table, max_id, 'time_stop');


    var calc = jr_date_diff(secondDate, firstDate, 'h');
    var day = jr_date_diff(secondDate, firstDate, 'd');

    // alert('first date' + firstDate + ' second date ' + secondDate + ' calc ' + calc + ' day ' + day + ' max_id ');

    calc = parseInt(calc);
    // var calc = jr_sum_subtable_column(table, 'delegation_time');

    jr_set_subtable_value(table, rowId, 'delegation_time', calc);

    // calc = jr_sum_subtable_column(table, 'delegation_time');
    calc = jr_get_subtable_value(table, rowId, 'delegation_time');

    // alert(calc);
    let delegation_type = jr_get_value('delegation_type');

    if (delegation_type == 'Krajowa') {

        if (calc < 8)
            totalAmount = 0;
        else if (calc >= 8 && calc <= 12)
            totalAmount = 15;
        else if (calc > 12 && calc <= 24)
            totalAmount = 30;
        else if (calc > 24) {
            var result = 0;

            var totalAmount = 0;

            totalAmount = day * 30;

            result = calc % 24;

            result = parseInt(result);

            if (result <= 8) {
                totalAmount += 15;
            }
            else if (result > 8) {
                totalAmount += 30;
            }

        }


    }
    else if (delegation_type == 'Zagraniczna') {

        // jr_set_subtable_value('STV_HOTEL_BOOKING',0, 'booking_from', firstDate);
        // jr_set_subtable_value('STV_HOTEL_BOOKING',0, 'booking_to', secondDate);

        let currency = jr_get_subtable_value(table, rowId, 'delegation_currency');

        let accommondation = jr_get_subtable_value(table, rowId, 'diet_accommondation');

        let diet = jr_get_subtable_value(table, rowId, 'delegation_diet');

        let country = jr_get_subtable_value(table, rowId, 'delegation_country2');

        // jr_notify_info('Pamiętaj!!! Dzienna dieta w kraju: ' + country + ' wynosi: '
        //     + diet + ' ' + currency + '. Maksymalny zwrot za nocleg wynosi: '
        //     + accommondation + ' ' + currency + '.', 50000);

        if (calc < 8)
            totalAmount = diet / 3;

        else if (calc >= 8 && calc <= 12)
            totalAmount = diet / 2;

        else if (calc > 12 && calc <= 24)
            totalAmount = diet;

        else if (calc > 24) {

            var result = 0;

            var totalAmount = 0;

            totalAmount = day * diet;

            result = calc % 24;

            result = parseInt(result);

            // if (result <= 8) {
            //     totalAmount += diet / 3;
            // }
            // else if (result > 8) {
            //     totalAmount += diet / 2;
            // }


        }

    }

    // jr_set_subtable_value(table, rowId, 'delegation_time', totalAmount);

    var total = jr_sum_subtable_column(table, 'delegation_time');

    jr_set_value('sum', totalAmount);
    // alert(totalAmount);
}




// Funkcja zerująca podsumowanie delegacji przy zmianie kraju

function deleteSummary() {

    jr_set_value('sum', '0');
    jr_set_value('sum2', '0');
    jr_set_value('sum3', '0');
    jr_set_value('sum4', '0');
    jr_set_value('sum5', '0');

}


// Funkcja obsługująca tabele Noclegi STV_HOTEL_BOOKING

function checkHotel(table, rowId) {

    let delegation_type = jr_get_value('delegation_type');

    var firstDate = jr_get_subtable_value(table, rowId, 'booking_from');

    var secondDate = jr_get_subtable_value(table, rowId, 'booking_to');

    var dayExpired = jr_date_diff(secondDate, firstDate, 'd');

    jr_set_subtable_value(table, rowId, 'booking_days', dayExpired);

    var booking_option = jr_get_subtable_value(table, rowId, 'booking_option');

    var booking_service = jr_get_subtable_value(table, rowId, 'booking_service');

    var discount;

    // Ustawienie parametrów w zależności od wybranej opcji noclegu
    if (booking_option == 1 || booking_option == 2) {

        jr_set_subtable_value(table, rowId, 'booking_cost', '');
        jr_set_subtable_value(table, rowId, 'booking_service', 0);

        booking_service = parseInt(booking_service);
    }


    let days = jr_get_subtable_value(table, rowId, 'booking_days');
    let cost = 0;



    // ALgorytm wiliczania odliczeń z diet ze względu na wybraną opcję wyżywienia
    if (booking_service == 1) {
        discount = 0.25;
        cost = 30 * days * discount;

    }
    else if (booking_service == 2) {
        discount = 0.5;
        cost = 30 * days * discount;
    }
    else if (booking_service == 3) {
        discount = 0.25;
        cost = 30 * days * discount;
    }
    else if (booking_service == 4) {
        discount = 1;
        cost = 30 * days * discount;
    }
    else if (booking_service == 5) {
        discount = 0.75;
        cost = 30 * days * discount;
    }
    else if (booking_service == 6) {
        discount = 0.75;
        cost = 30 * days * discount;
    }
    else if (booking_service == 7) {
        discount = 0.5;
        cost = 30 * days * discount;
    }




    jr_set_subtable_value(table, rowId, 'booking_test', cost);


    let booking_days = jr_get_subtable_value(table, rowId, 'booking_days');

    let booking_cost = jr_get_subtable_value(table, rowId, 'booking_cost');

    let sum2 = jr_sum_subtable_column(table,'booking_test');

    jr_set_value('sum2', sum2);

    // Wyliczenie całkowitych kosztów za nocleg
    let totalCost = booking_days * booking_cost;

    jr_set_subtable_value(table, rowId, 'booking_total_cost', totalCost);

    let sum3 = jr_sum_subtable_column(table,'booking_total_cost' );

    jr_set_value('sum3', sum3);


}

// Funkcja sprawdzająca całkowity koszt, i czy doszło do przekroczenia limitu, ora wyliczjąca ryczałt

// function checkOverPaidBooking() {
//
//
//     let days = jr_get_subtable_value('STV_HOTEL_BOOKING', 0, 'booking_days');
//     let cost = jr_get_subtable_value('STV_HOTEL_BOOKING', 0, 'booking_cost');
//     let booking_option = jr_get_subtable_value('STV_HOTEL_BOOKING', 0, 'booking_option');
//     let total_cost = 0;
//     let fee = 45;
//
//     if (booking_option == 0) {
//
//         total_cost = cost * days;
//
//     }
//     else if (booking_option == 1) {
//
//         total_cost = days * fee;
//
//     }
//
//     if(cost > 180) {
//
//         alert('Przekroczono limit.');
//     }
//
//     jr_set_subtable_value('STV_HOTEL_BOOKING', 0, 'booking_total_cost', total_cost);
//
//     jr_set_value('sum3', total_cost);
//
//
// }

// Obsługa etapu transportu

function TravelLoop()
{
    jr_loop_table('STV_BOOK_FLY', checkTravel);
}

function checkTravel(table, rowId){

    let sum4 = jr_sum_subtable_column(table,'fly_price' );
    jr_set_value('sum4', sum4);


}

// Obsługa etapu pozostałych kosztów
function allCostLoop()
{
    jr_loop_table('STV_OTHER_COST', checkAllCost);
}

function checkAllCost(table, rowId){

    let sum5= jr_sum_subtable_column(table, 'cost_price');
    jr_set_value('sum5', sum5);

}

// Lista rodzaju delegacji
function delegationType(){

    let delegation_type =  jr_get_value('delegation_type');
    // alert('W funkcji' + jr_get_value('delegation_option'));
    if(delegation_type == 'Krajowa'){

        deleteSummary();

        jr_show('delegationType');
        jr_show('delegationDetails');
        jr_show('hotel');
        jr_show('transport');
        jr_show('other_costs');
        jr_hide('info2');
        jr_show('info1');
        jr_hide('info3');
        jr_hide_subtable_column('STV_DELEGATION_DETAIL', 'delegation_country');
        jr_hide_subtable_column('STV_DELEGATION_DETAIL', 'delegation_country2');

    }
    else if(delegation_type == 'Zagraniczna'){

        deleteSummary();

        jr_show('delegationType');
        jr_show('delegationDetails');
        jr_show('hotel');
        jr_show('transport');
        jr_show('other_costs');
        jr_hide('info2');
        jr_show('info3');
        jr_hide('info1');
        jr_show_subtable_column('STV_DELEGATION_DETAIL', 'delegation_country');
        jr_show_subtable_column('STV_DELEGATION_DETAIL', 'delegation_country2');

    }

}


function  delegationOption() {

    let delegation_option = jr_get_value('delegation_option');

    if(delegation_option == 'Serwisowy') {

        jr_show('info4');
        jr_hide('info5');

    }
    else if(delegation_option == 'Planowy') {

        jr_show('info5');
        jr_hide('info4');
    }


}

function transportLoop()
{
    jr_loop_table('STV_BOOK_FLY', transportOption);
}

function transportOption(table, rowId){


    let transportOption = jr_get_subtable_value(table, rowId, 'fly_options');




    if(transportOption == '0') {

        document.getElementById(table + '_fly_price_' + rowId).disabled = true;

        // jr_show_subtable_column(table, 'fly_registry');

    }
    else if(transportOption == '1'){

        jr_show('row25');
        jr_show('row26');
        jr_show('row27');

        // jr_show_subtable_column(table, 'fly_registry');

    }
    else if(transportOption == '2'){

        jr_hide('row25');
        jr_hide('row26');
        jr_hide('row27');

        document.getElementById(table + '_fly_registry_' + rowId).disabled = true;


        // jr_hide_subtable_column(table, 'fly_registry');

    }
    else if(transportOption == '3'){

        jr_hide('row25');
        jr_hide('row26');
        jr_hide('row27');
        document.getElementById(table + '_fly_registry_' + rowId).disabled = true;


        // jr_hide_subtable_column(table, 'fly_registry');

    }

}

function instalmentOption(){

    let instalmentOption = jr_get_value('instalment_need');

    if(instalmentOption == 'Tak'){

        jr_show('info7');
        jr_show('instalment_number');
        jr_show('instalment_account');
    }
    else{

        jr_hide('info7');
        jr_hide('instalment_number');
        jr_hide('instalment_account');
    }

}

function rentOption(){

    let carpPrice = 0;

    let rentOption = jr_get_value('carList');

    if(rentOption == 0){

        carpPrice = 0.5214;
    }
    else if(rentOption == 1){

        carpPrice = 0.8358;

    }
    else if(rentOption == 2){

        carpPrice = 0.2302;
    }
    else if(rentOption == 3){

        carpPrice = 0.1382;
    }


    jr_set_value('licence_transport', carpPrice);



}

function rentLoop()
{
    jr_loop_table('STV_CAR_RENT', carRent);
}

function  carRent(table, rowId) {


    let price = jr_get_value('licence_transport');
    let sum = 0;
    jr_set_subtable_value(table, rowId, 'ca_price', price);

    let kilometers = jr_get_subtable_value(table, rowId, 'car_km');

    sum = price * kilometers;

    jr_set_subtable_value(table, rowId, 'car_sum', sum);

    let total_kilometers = jr_sum_subtable_column(table,'car_km');

    let total_sum = jr_sum_subtable_column(table,'car_sum');

    jr_set_value('total_kilometers', total_kilometers);
    jr_set_value('total_sum', total_sum);

    let suma = jr_get_value('sum4');
    suma = suma + total_sum;
    jr_set_value('sum4', suma);

}

function wait_instalment(){

    setTimeout(instalment_check, 1500);

}

function instalment_check() {


    let user = jr_get_value('employee');

    let user_instalment = jr_get_value('instalment_user');

    if(user == user_instalment) {

        jr_show('info10');
        jr_hide('info9');

    }

    else if(user != user_instalment) {

        jr_show('info9');
        jr_hide('info10');

    }


}

function data_check(){

    let max_deleagation = jr_get_subtable_max_id('STV_DELEGATION_DETAIL');

    let delegation_start = jr_get_subtable_value('STV_DELEGATION_DETAIL', max_deleagation, 'time_start');
    let delegation_stop = jr_get_subtable_value('STV_DELEGATION_DETAIL', max_deleagation, 'time_stop');

    let max_book = jr_get_subtable_max_id('STV_BOOK_FLY');
    let delegation_start = jr_get_subtable_value('STV_BOOK_FLY', max_book, 'fly_start');
    let delegation_stop = jr_get_subtable_value('STV_BOOK_FLY', max_book, 'fly_return');

    let max_hotel = jr_get_subtable_max_id('STV_HOTEL_BOOKING');
    let delegation_start = jr_get_subtable_value('STV_HOTEL_BOOKING', max_hotel, 'booking_from');
    let delegation_stop = jr_get_subtable_value('STV_HOTEL_BOOKING', max_hotel, 'booking_to');




}

