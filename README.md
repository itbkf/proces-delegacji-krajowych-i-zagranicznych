Informacje o projekcie
===
Sponsor: Piotr Andrzejewski     <p.andrzejewski@bkf.pl>
===
Leader: Łukasz Filipik      <l.filipiak@bkf.pl>
===

1. Uzasadnienie realizacji projektu
---
- Ze względu na zbyt długi okres rozliczania delegacji,
- Papierowy obiekg dokumentów który powodował zbyt duże opóźnienia,
- Brak centralnej ewidencji delegacji

2. Mierzalne cele i kryteria sukcesu projektu
---
 - Po sprawdzeniu i analizie w Google Data Studio 

3. Opis projektu
---
Cele
Utworzenie w JobRouter kroku pierwszego “Formularz polecenia wyjazdu służbowego”:

Pracownik będzie miał dostęp do procedury tj. Jak zamawiać hotele, jak będzie rozliczana delegacja, jakie są limity budżetowe 
Pracownik wybiera się z listy uzupełnianej ( kontrolka sql autocomplete wyciągającej z tabeli  jr_users listę pracowników ),
Skrypt automatycznie wypełnia pola Przełożony oraz Dział,
Pracownik wybiera rodzaj delegacji: Krajowa czy Zagraniczna,
Pracownik wybiera czy jest to delegacja Planowa ( tryb normalny ) czy wyjazd serwisowy (tryb przyśpieszony).
W zależności od wyborów dokonywanych przez pracownika będą widoczne odpowiednie scenariusze dla danej sytuacji opisane w dalszych punktach.

1.1 Scenariusz: delegacja krajowa, wyjazd serwisowy.

Pracownik wypełnia Miejsce wyjazdu oraz Miejsce docelowe jeżeli jest ich kilka to poprzez przyciśnięcie przycisku + ( plus ) dodaje kolejne.
Pracownik wypełnia przewidywany czas wyjazdu oraz przyjazdu, cel podróży. Orientacyjne dokładne dane zostaną uzupełnione po zakończeniu delegacji.
Jeżeli delegacja jest dłuższa niż jeden dzień pracownik wybiera opcje noclegu:
W własnym zakresie,
Hotel
Nocleg zapewnia kontrahent

Jeżeli wybrana została opcja Hotel, pracownik musi wpisać koszt noclegu za dobę, czy wykupuje śniadanie/obiad/kolację, jeżeli hotel wybrany przez serwis np. Booking.com to opcjonalnie można wkleić link do oferty.
Pracowni wybiera również środek transportu:

Samochód służbowy,
Samochód zastępczy,
Przejazd pociągiem/autokarem
?? Samolot ??

Jeżeli wybrana została opcja Samochód służbowy to skrypt wybiera który samochód jest przypisany do którego serwisanta.
Pracownik wypełnia również rubrykę koszty dodatkowe np. Zakup oleju, tankowanie itp.

1.2 Scenariusz: delegacja krajowa, wyjazd planowany

Pracownik wypełnia Miejsce wyjazdu oraz Miejsce docelowe jeżeli jest ich kilka to poprzez przyciśnięcie przycisku + ( plus ) dodaje kolejne.
Pracownik wypełnia czas wyjazdu oraz przyjazdu.
 Jeżeli delegacja jest dłuższa niż jeden dzień pracownik wybiera opcje noclegu:
W własnym zakresie,
Hotel

Jeżeli wybrana została opcja Hotel, pracownik musi wpisać koszt noclegu za dobę, czy wykupuje śniadanie/obiad/kolację, jeżeli hotel wybrany przez serwis np. Booking.com to opcjonalnie można wkleić link do oferty.
Pracownik wypełnia agendę tego co będzie robił podczas podróży służbowej,  jeżeli delegacja trwa dłużej niż jeden dzień. Pracownik wypełnia agendę dla każdego dnia z osobna.
Pracowni wybiera również środek transportu:
Samochód służbowy,
Samochód zastępczy,
Przejazd pociągiem/autokarem
?? Samolot ??

Jeżeli transport pociąg/autokar to wpisuje koszt biletów.
Pracownik wypełnia również rubrykę/tabelę koszty dodatkowe np. Materiały marketingowe itp. 
1.3 Scenariusz: delegacja zagraniczna, wyjazd serwisowy/planowany

Pracownik wypełnia Miejsce wyjazdu oraz Miejsce docelowe, Kraj wyjazdu, kraj przyjazdu jeżeli jest ich kilka to poprzez przyciśnięcie przycisku + ( plus ) dodaje kolejne.
Pracownik wypełnia czas wyjazdu oraz przyjazdu.
Jeżeli delegacja jest dłuższa niż jeden dzień pracownik wybiera opcje noclegu:
W własnym zakresie,
Hotel
Jeżeli wybrana została opcja Hotel, pracownik musi wpisać koszt noclegu za dobę, czy wykupuje śniadanie/obiad/kolację, wkleja obowiązkowo link do oferty.
Pracowni wybiera również środek transportu:

Samochód służbowy,
Samochód  wynajęty,
Przejazd pociągiem/autokarem
Samolot

Jeżeli została wybrana opcja samolot, to pracownik obowiązkowo wpisuje link do oferty przelotu i powrotu oraz ceny za bilety w jedną i drugą stronę,
Jeżeli pracownik wybierze opcję dowóz na lotnisko, odbiór z lotniska, wpisuje odpowiednie ceny za usługę oraz w miarę możliwości wkleja link z ofertą,
Pracownik wybiera również czy na miejscu będzie poruszał się samochodem wynajętym czy lokalnymi środkami transportu. Jeżeli wybierze samochód należy wkleić link do oferty oraz wpisać koszt za wynajem/doba oraz na jaki okres samochód będzie wypożyczany.
Pracownik wypełnia również w miarę możliwości dodatkowe sekcję dodatkowe opłaty za delegację. Np. koszt zakupu materiałów marketingowych itp.
Pracownik wypełnia agendę tego co będzie robił podczas podróży służbowej,  jeżeli delegacja trwa dłużej niż jeden dzień. Pracownik wypełnia agendę dla każdego dnia z osobna.

2. Krok akceptacji delegacji przez Kierownika Działu

Kierownik dostaję formularz wstępny wypełniony przez pracownika,
W zależności od typu scenariusza delegacji skrypt zlicza przewidywane koszty podróży,
Jeżeli jest to podróż zagraniczna i jakieś sumy zostały podane w walucie innej niż PLN to program automatycznie przelicza z obcej waluty na polską w oparciu o kurs w danym dniu pobierany przez API NBP.
Jeżeli zostały dodane linki do ofert -  to poprzez kliknięcie pokaż przeniesiemy się do strony z ofertą bądź możemy ją wyświetlić w iframe.
Kierownik może zaakceptować delegację bądź cofnąć ją do poprawy.

3. Uzupełnienie danych po zakończeniu delegacji przez pracownika.

Pracownik po zakończeniu delegacji jest zobowiązany w terminie nie dłuższym niż 14 dni uzupełnić wszelkie informację potrzebne do rozliczenia delegacji jak również dostarczyć wszelkie dokumenty potwierdzające koszty poniesione przez pracodawcę np. Faktury za noclegi, dodatkowe koszty związane z podróżą. 
Część tych danych pracownik wypełnia już w kroku pierwszym, jeżeli któryś danych nie uzupełnił bądź nie mógł ( wyjazd serwisowy) lub uległy one zmianie to wpisuje je teraz. Np:

Dokładny rozliczenie na czas wyjazdu, czas przyjazdu np. 8.15 - 16.15,
Dodatkowe koszty np tankowanie samochodu

Pracownik dodatkowo załącza skany, bądź zdjęcia dokumentów z delegacji,


 4. Weryfikacja danych przez dział Controllingu.

Dział controllingu sprawdza na podstawie dostarczonych dokumentów oraz programu Finder czy informacje wprowadzone przez pracownika są zgodne z prawdą,
W razie wykrycia nieprawidłowości pracownik controllingu odsyła do kierownika działu odpowiedzialnego za daną delegację krok z prośbą poprawienia danych bądź dostarczenia brakujących dokumentów
Jeżeli wszystko jest w porządku pracownik wysyła krok dalej.
	
5. Weryfikacja delegacji przez K. F-A.

Klaudia Fedorowicz - Andrzejewska sprawdza poprawność delegacji i w razie niezgodności może odesłać ją do poprawy przez controlling bądź zaakceptować i wysłać do Działu Księgowości.
Proces automatycznie wylicza diety oraz sumuje pozostałe koszty więc w tym miejscu widzimy już całkowity koszt delegacji.

6. Weryfikacja delegacji przez księgowość i ostateczne rozliczenie delegacji.

Księgowość otrzymuje krok ze wszystkimi danymi potrzebnymi do rozliczenia delegacji oraz wyliczone diety dla pracownika na podstawie rozporządzenia.
Księgowość będzie mogła modyfikować dane
Księgowość uzupełnia dane czy delegacja została rozliczona, jeżeli tak to kończy to proces jeżeli nie to krok wraca do controllingu z informacją o niezgodnościach.


Założenia ( do przedyskutowania )
Przy delegacjach krajowych pracownikowi przysługuje dzienna dieta w wysokości 30 zł oraz 180 zł opłaty za nocleg na dobę.
Śniadanie pomniejsza dietę o 25%, obiad o 50%, kolacja o 25%.
Przy jednej dobie jeżeli wyjazd trwał do 8 godzin pracownik nie otrzymuje diety, od 8 do 12 godzin 50% diety, powyżej 12 godzin 100% diety.
Jeżeli podróż krajowa trwa dłużej niż dobę, za każdą dobę przysługuje dieta w pełnej wysokości, a za niepełną, ale rozpoczętą dobę: do 8 godz. 50% powyżej 100%
Ekwiwalent za brak noclegu/ nocleg na własny koszt 150% diety czyli 45 zł za dobę.
Wysokość diet zagranicznych określa Rozporządzenie Ministra Gospodarki i Pracy z dnia 14 września 2005 r. Ich wysokość oczywiście różni się w zależności od kraju, w którym odbywa się podróż służbowa. Za każdą dobę zagranicznej podróży służbowej przysługuje dieta w pełnej wysokości. Za niepełną dobę podróży: do 8 godzin - przysługuje 1/3 diety zagranicznej, ponad 8 do 12 godzin - przysługuje 1/2 diety zagranicznej, ponad 12 godzin - przysługuje dieta w pełnej wysokości.
 Pracodawca zapewnia częściowe wyżywienie:
śniadanie – 15% diety,
obiad – 30% diety,
kolacja - 30% diety.


4. Wstępne ryzyka projektu
---
- Nie mieliśmy wcześniej  zadnego oprogramowania do zarządzania delegacjami więc analiza procesu musi być zastosowana na kązdym jego etapie



5. Zadania powiązane
---




